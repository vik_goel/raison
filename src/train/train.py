from arc_dataset import ArcDataset, DatasetType
import numpy as np
import torch
import torch.nn as nn
from torchsummary import summary

task_name = '0a938d79'
#task_name = '007bbfb7'
#task_name = '00d62c1b'
train_batch_size = 128
validation_batch_size = 50
learning_rate = 1e-4
epochs = 100000

train_dataset = ArcDataset(task_name, DatasetType.TRAIN)
train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=train_batch_size,
        shuffle=True,
        num_workers=0)

validation_dataset = ArcDataset(task_name, DatasetType.VALIDATION)
validation_loader = torch.utils.data.DataLoader(
        validation_dataset,
        batch_size=validation_batch_size,
        shuffle=False,
        num_workers=0)
def validation_data_generator_fn():
    while True:
        for batch in validation_loader:
            yield batch
validation_data_generator = validation_data_generator_fn()

# TODO: Padding should use the correct grid coordinates.
#       This could be achieved by using valid padding on an input which is large enough to shrink down to the right size.
# TODO: Add skip/dense connections

num_channels = 12
model = nn.Sequential(
    nn.Conv2d(in_channels=train_dataset.NUM_COLOURS+3, out_channels=num_channels, kernel_size=3, padding=1),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),

    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=1, dilation=1),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),

    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=1, dilation=1),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),

    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),

    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=1, dilation=1),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),

    nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=2, dilation=2),
    nn.ReLU(),
    nn.Conv2d(in_channels=num_channels, out_channels=train_dataset.NUM_COLOURS+1, kernel_size=3, padding=1, dilation=1),
)
summary(model, (train_dataset.NUM_COLOURS+3, train_dataset.MAX_GRID_SIZE, train_dataset.MAX_GRID_SIZE))

loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

for epoch in range(epochs):
    print('\nEpoch:', epoch, end='')

    # Train loop.
    for i, (input_images, output_images) in enumerate(train_loader):
        pred = model(input_images)
        loss = loss_fn(pred, output_images)

        if i == 0:
            print(', Train loss:', loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    # Validation loop.
    losses = []
    input_images, output_images = next(validation_data_generator)
    pred = model(input_images)
    pred_idx = pred.argmax(dim=1)
    invalid_output_size_percentage = (output_images == 0).float().mean()
    matches = (torch.logical_or(pred_idx == output_images, output_images == 0)).float()

    # Correct mean every square of the output grid is predicted correctly.
    correct = matches.min(dim=-1)[0].min(dim=-1)[0]

    # All invalid cells are marked as accurate. They need to be removed before getting a true accuracy.
    # TODO: Add another accuracy measure ensuring that the model correctly predicts when a cell
    #       is out of bounds.
    accuracy = (matches.mean() - invalid_output_size_percentage) / (1.0 - invalid_output_size_percentage)

    print('Accuracy:', accuracy.item() * 100.0)
    print('Correct:', correct.mean().item() * 100.0)
    loss = loss_fn(pred, output_images)
    losses.append(loss.item())

    print('Validation loss:', np.mean(losses))

