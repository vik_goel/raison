from enum import Enum
import json
import numpy as np
import torch
from torch.utils.data import Dataset

class DatasetType(Enum):
    TRAIN = 'train'
    VALIDATION = 'validation'
    TEST = 'test'


class ArcDataset(Dataset):

    NUM_COLOURS = 10
    MAX_GRID_SIZE = 30

    def __init__(self, task_name: str, dataset_type: DatasetType):
        super(Dataset, self).__init__()
        with open('./data/%s.json' % task_name, 'r') as f:
            task = json.load(f)[dataset_type.value]
        print('{} dataset has {} examples'.format(dataset_type.value, len(task)))

        self.inputs = list(map(lambda x: self._encode_input_grid(x['input']), task))
        self.outputs = list(map(lambda x: self._encode_output_grid(x['output']), task))
        self.dataset_type = dataset_type

        #max_len = 100
        #self.inputs = self.inputs[:max_len]
        #self.outputs = self.outputs[:max_len]
        #self.outputs = [torch.tensor([[0]*9]*9)]



    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, idx: int):
        return self.inputs[idx], self.outputs[idx]

    # First 2 input positions store the x, y pos.
    # Next input position encodes whether this is a valid grid cell (or if its out of bounds).
    # Next NUM_COLOURS grid cells store a one-hot encoding of the colour.
    def _encode_input_grid(self, grid):
        output = np.zeros([self.NUM_COLOURS + 3, self.MAX_GRID_SIZE, self.MAX_GRID_SIZE], np.float32)

        for row_idx in range(self.MAX_GRID_SIZE):
            for col_idx in range(self.MAX_GRID_SIZE):
                output[0][row_idx][col_idx] = (row_idx + 1.0) / self.MAX_GRID_SIZE
                output[1][row_idx][col_idx] = (col_idx + 1.0) / self.MAX_GRID_SIZE

                if row_idx >= len(grid) or col_idx >= len(grid[0]):
                    output[2][row_idx][col_idx] = 1.0

        for row_idx, row in enumerate(grid):
            for col_idx, val in enumerate(row):
                assert val >= 0
                assert val < self.NUM_COLOURS
                assert output[2][row_idx][col_idx] == 0.0
                output[val + 3][row_idx][col_idx] = 1.0

        return torch.from_numpy(output)


    def _encode_output_grid(self, grid):
        output = np.zeros([self.MAX_GRID_SIZE, self.MAX_GRID_SIZE], np.int64)
        for row_idx, row in enumerate(grid):
            for col_idx, val in enumerate(row):
                output[row_idx][col_idx] = val+1
        return torch.from_numpy(output)

