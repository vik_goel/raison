import unittest
import helper 

def test_permute_2x2_grid():
    permutable_grid = [ [[0, 1, 2], [2, 3]],
                        [[3], [1, 3]] ]
    grids = list(helper.permute_grid(permutable_grid))
    expected = [
        [[0, 2], [3, 1]],
        [[0, 2], [3, 3]],
        [[0, 3], [3, 1]],
        [[0, 3], [3, 3]],
        [[1, 2], [3, 1]],
        [[1, 2], [3, 3]],
        [[1, 3], [3, 1]],
        [[1, 3], [3, 3]],
        [[2, 2], [3, 1]],
        [[2, 2], [3, 3]],
        [[2, 3], [3, 1]],
        [[2, 3], [3, 3]],
    ]
    
    assert len(grids) == len(expected)
    for e in expected:
        assert e in grids

