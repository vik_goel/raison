from colours import Colour
import constants
from copy import deepcopy
import grid_utils
import itertools
import output_task
import random

'''
1. Pick a grid size
2. Create a black grid with that size
3. Set some inner squares of the grid to be yellow
4. Surrond all yellow squares with green squares (this is the output grid)
5. Remove the yellow squares (this is the input grid)
'''

def get_grid_size():
    return random.randint(3, constants.MAX_GRID_SIZE)

def get_yellow_grid():
    grid_size = get_grid_size()
    grid = grid_utils.create_grid(grid_size, grid_size)

    possible_yellow_square_locations = list(itertools.product(range(1, grid_size-1), repeat=2))
    yellow_square_count = random.randint(1, len(possible_yellow_square_locations))
    yellow_square_locations = random.sample(possible_yellow_square_locations, yellow_square_count)
    for row, col in yellow_square_locations:
        grid[row][col] = Colour.YELLOW

    return grid

def get_output_grid(yellow_grid):
    grid = deepcopy(yellow_grid)
    for row_idx, row in enumerate(grid):
        for col_idx, colour in enumerate(row):
            if colour == Colour.BLACK:
                if Colour.YELLOW in grid_utils.get_cardinal_neighbours(yellow_grid, row_idx, col_idx):
                    grid[row_idx][col_idx] = Colour.GREEN
    return grid

def get_input_grid(output_grid):
    grid = deepcopy(output_grid)
    grid_utils.substitute_colour(grid, Colour.YELLOW, Colour.BLACK)
    return grid

def example_generator():
    while True:
        yellow_grid = get_yellow_grid()
        output_grid = get_output_grid(yellow_grid)
        input_grid = get_input_grid(output_grid)
        yield input_grid, output_grid

if __name__ == '__main__':
    output_task.write_output('00d62c1b', example_generator(), constants.MAX_EXAMPLES)

