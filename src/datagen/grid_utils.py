from colours import Colour

def create_grid(rows, cols, fill_colour=Colour.BLACK):
    return [[fill_colour] * cols for _ in range(rows)]

def get_cardinal_neighbours(grid, row, col):
    output = []
    if row > 0: output.append(grid[row-1][col])
    if row + 1 < len(grid): output.append(grid[row+1][col])
    if col > 0: output.append(grid[row][col-1])
    if col + 1 < len(grid[0]): output.append(grid[row][col+1])
    return output

def substitute_colour(grid, find_colour, replace_colour):
    for row_idx, row in enumerate(grid):
        for col_idx, colour in enumerate(row):
            if colour == find_colour:
                grid[row_idx][col_idx] = replace_colour

def copy_grid(dst_grid, src_grid, dst_row_start, dst_col_start):
    for row_idx, row in enumerate(src_grid):
        for col_idx, colour in enumerate(row):
            dst_grid[dst_row_start + row_idx][dst_col_start + col_idx] = colour

def set_row(grid, row, colour):
    for col in range(len(grid[row])):
        grid[row][col] = colour

def set_col(grid, col, colour):
    for row in range(len(grid)):
        grid[row][col] = colour

def transpose(grid):
    rows, cols = len(grid), len(grid[0])
    output = [[0] * rows for _ in range(cols)]
    for row in range(rows):
        for col in range(cols):
            output[col][row] = grid[row][col]
    return output
