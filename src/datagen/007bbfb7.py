'''
1. Pick a colour, C
2. Create a 3x3 grid as the input
    - assign each square C or black
3. Create the output by tiling where needed
'''

from colours import Colour
import grid_utils
import output_task
import permute

def example_generator():
    all_grids = []
    for colour in Colour:
        if colour == Colour.BLACK: continue

        possible = [colour, Colour.BLACK]
        grid = [[possible]*3] * 3
        permutations = permute.permute_grid(grid)
        for perm in permutations:
            yield perm, get_output_grid(perm)


def get_output_grid(input_grid):
    output_grid = grid_utils.create_grid(9, 9)
    for row in range(3):
        for col in range(3):
            if input_grid[row][col] != Colour.BLACK:
                grid_utils.copy_grid(output_grid, input_grid, row * 3, col * 3)
    return output_grid


if __name__ == '__main__':
    output_task.write_output('007bbfb7', example_generator())

