import json
import random

VALIDATION_PERCENTAGE = 0.2

def write_output(task_name, example_generator, max_examples=None):
    test_grids = _read_test_grids(task_name)
    train = []

    for example_idx, (input_grid, output_grid) in enumerate(example_generator):
        if _is_test_grid(input_grid, test_grids): continue
        train.append({'input': _encode_grid(input_grid), 'output': _encode_grid(output_grid)})

        if max_examples is not None and example_idx >= max_examples:
            break

    random.shuffle(train)
    validation_cutoff = int(VALIDATION_PERCENTAGE * len(train))
    validation = train[:validation_cutoff]
    train = train[validation_cutoff:]

    output = {'train': train, 'validation': validation, 'test': test_grids}
    with open('./data/%s.json' % task_name, 'w') as f:
        json.dump(output, f)


def _encode_grid(grid):
    result = []
    for row in grid:
        result_row = []
        result.append(result_row)
        for colour in row:
            result_row.append(colour.value)
    return result


def _read_test_grids(task_name):
    with open('./ARC/data/training/%s.json' % task_name, 'r') as f:
        task = json.load(f)
    return task['test']


# Examples in the training and testing sets should not overlap.
# This function removes any overlapping examples.
def _is_test_grid(input_grid, test_grids):
    for test_grid in test_grids:
        if input_grid == test_grid['input']:
            return True
    return False

