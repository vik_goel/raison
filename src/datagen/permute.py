'''
This function takes in a 3D grid as input. Eg.
[ [[0, 1], [2, 3]]
  [[0]   , [1, 2, 3]]]
It outputs the list of all possible 2D grids where each cell of the 3D grid is a list of possible options.
Eg. One possible output for the above grid would be
[ [0 2]
  [0 1]]
A list of 12 grids would be returned for the example above.
'''
def permute_grid(permutable_grid):
    flattened_grid = _flatten_permutable_grid(permutable_grid)
    permutations = _permute_flattened_grid(flattened_grid)
    return map(lambda perm: _unflatten_grid(len(permutable_grid), perm), permutations)


def _unflatten_grid(num_rows, flattened_grid):
    grid = []
    num_cols = len(flattened_grid) // num_rows

    for row_index in range(num_rows):
        start_index = row_index * num_cols
        end_index = (row_index + 1) * num_cols
        grid.append(flattened_grid[start_index:end_index])

    return grid


def _permute_flattened_grid(flattened_grid):
    if len(flattened_grid) == 0: return [[]]

    prefixes = _permute_flattened_grid(flattened_grid[:-1])
    result = []
    for suffix in flattened_grid[-1]:
        for prefix in prefixes:
            result.append(prefix + [suffix])
    return result

'''
Converts a 3D permutable grid to a 2D array.
Each item of the array stores the possible values for a grid cell.
'''
def _flatten_permutable_grid(permutable_grid):
    result = []
    for row in permutable_grid:
        for cell in row:
            result.append(cell)
    return result

