from enum import Enum

class Colour(Enum):
    BLACK = 0
    DARK_BLUE = 1
    LIGHT_RED = 2
    GREEN = 3
    YELLOW = 4
    GREY = 5
    MAGENTA = 6
    ORANGE = 7
    LIGHT_BLUE = 8
    DARK_RED = 9

NON_BLACK_COLOURS = [c for c in Colour if c != Colour.BLACK]
