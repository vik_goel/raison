import constants
from colours import Colour, NON_BLACK_COLOURS
from copy import deepcopy
import grid_utils
import output_task
import random


def get_grid():
    rows = random.randint(3, constants.MAX_GRID_SIZE)
    cols = random.randint(3, constants.MAX_GRID_SIZE)
    return grid_utils.create_grid(rows, cols)


def get_vertical_grid():
    input_grid = get_grid()
    spacing = min(10, random.randint(1, len(input_grid) - 2))
    min_row = random.randint(0, len(input_grid)-spacing-1)
    colours = random.sample(NON_BLACK_COLOURS, 2)

    input_grid[min_row][0] = colours[0]
    input_grid[min_row + spacing][-1] = colours[1]

    output_grid = deepcopy(input_grid)
    row = min_row
    while True:
        for colour in colours:
            grid_utils.set_row(output_grid, row, colour)
            row += spacing
            if row >= len(output_grid):
                return input_grid, output_grid


def example_generator():
    while True:
        input_grid, output_grid = get_vertical_grid()
        if bool(random.getrandbits(1)):
            input_grid = grid_utils.transpose(input_grid)
            output_grid = grid_utils.transpose(output_grid)
        yield input_grid, output_grid


if __name__ == '__main__':
    output_task.write_output('0a938d79', example_generator(), constants.MAX_EXAMPLES)


